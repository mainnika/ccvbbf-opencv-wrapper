/* BBF Face Detector */

#include <iostream>
#include <sstream>
#include <vector>
#include <cctype>
#include <string>
#include <ctime>

#include <opencv2/highgui.hpp>

#include "ccvbbfwrapper.h"

int main(int argc, char** argv) {

    CV_Assert(argc >= 3);

    cv::Mat loadimg = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);
    std::vector<cv::Rect> faces;
    CcvBbfWrapper bbf(argv[2]);

    std::clock_t elapsed_time = std::clock();
    bbf.detect(loadimg, faces);
    elapsed_time = std::clock() - elapsed_time;

    std::for_each(faces.begin(), faces.end(), [&loadimg](cv::Rect face) {
        cv::rectangle(loadimg, face, cv::Scalar(255, 0, 0), 2);
    });

    std::stringstream ss;
    ss << "Result: " << faces.size() << " faces, " << elapsed_time << " ms";

    cv::imshow(ss.str(), loadimg);

    cv::waitKey();

    return 0;
}
