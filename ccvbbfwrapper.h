/* 
 * File:   ccvbbfwrapper.h
 * Author: mainnika
 *
 * Created on March 6, 2014, 11:08 AM
 */

#ifndef CCVBBFWRAPPER_H
#define	CCVBBFWRAPPER_H

#include <opencv2/core.hpp>

extern "C" {
#include <ccv.h>
}

class CcvBbfWrapper {
    ccv_dense_matrix_t* _dense_mat;
    uchar* _dense_mat_ptr;
    ccv_bbf_classifier_cascade_t* _cascade;

public:

    CcvBbfWrapper(const cv::String& filename);
    virtual ~CcvBbfWrapper();
    void detect(const cv::Mat& image,
            std::vector<cv::Rect>& objects,
            int interval = 5,
            int min_neighbors = 2,
            int flags = 0,
            int accurate = 1,
            cv::Size size = cv::Size(24, 24));
};

#endif	/* CCVBBFWRAPPER_H */

