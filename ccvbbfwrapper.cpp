/* 
 * File:   ccvbbfwrapper.cpp
 * Author: mainnika
 * 
 * Created on March 6, 2014, 11:08 AM
 */

#include "ccvbbfwrapper.h"

void CcvBbfWrapper::detect(const cv::Mat& image,
        std::vector<cv::Rect>& objects,
        int interval,
        int min_neighbors,
        int flags,
        int accurate,
        cv::Size size) {

    CV_Assert(image.type() == CV_8UC1);

    _dense_mat = ccv_dense_matrix_new(image.rows, image.cols, CCV_8U | CCV_C1, 0, 0);
    _dense_mat_ptr = _dense_mat->data.u8;

    ccv_bbf_param_t _params = {
        interval,
        min_neighbors,
        flags,
        accurate,
        {
            size.width,
            size.height
        }
    };

    for (int i = 0; i < image.rows; i++) {
        memcpy(_dense_mat_ptr, [&image](int y) { // looks like js callback
            return image.data + image.step.p[0] * y;
        }(i), image.cols);
        _dense_mat_ptr += _dense_mat->step;
    }

    ccv_array_t* ccv_obj = ccv_bbf_detect_objects(_dense_mat, &_cascade, 1, _params);

    objects.clear();
    for (int i = 0; i < ccv_obj->rnum; i++) {
        ccv_comp_t* comp = (ccv_comp_t*) ccv_array_get(ccv_obj, i);//static_cast<ccv_comp_t*> (ccv_obj->data) + static_cast<size_t> (ccv_obj->rsize * i);
        objects.push_back(cv::Rect(comp->rect.x, comp->rect.y, comp->rect.width, comp->rect.height));
    }

    ccv_array_free(ccv_obj);
    ccv_matrix_free(_dense_mat);
    _dense_mat_ptr = NULL;
    _dense_mat = NULL;
}

CcvBbfWrapper::CcvBbfWrapper(const cv::String& filename) {
    ccv_enable_default_cache();
    _cascade = ccv_bbf_read_classifier_cascade(filename.c_str());
    _dense_mat = NULL;
}

CcvBbfWrapper::~CcvBbfWrapper() {
    ccv_bbf_classifier_cascade_free(_cascade);
    ccv_disable_cache();
}
